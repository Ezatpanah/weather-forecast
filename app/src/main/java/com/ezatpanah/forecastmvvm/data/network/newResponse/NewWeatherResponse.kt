package com.ezatpanah.forecastmvvm.data.network.newResponse


import com.google.gson.annotations.SerializedName

data class NewWeatherResponse(
        @SerializedName("clouds")
    var clouds: Clouds ,
        @SerializedName("dt")
    var dt: Int = 0,
        @SerializedName("dt_txt")
    var dtTxt: String = "",
        @SerializedName("main")
    var main: MainForecast,
        @SerializedName("sys")
    var sys: SysForecast,
        @SerializedName("weather")
    var weather: List<Weather> ,
        @SerializedName("wind")
    var wind: Wind
)