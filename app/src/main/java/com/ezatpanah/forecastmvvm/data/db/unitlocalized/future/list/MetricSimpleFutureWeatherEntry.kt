package com.ezatpanah.forecastmvvm.data.db.unitlocalized.future.list

import androidx.room.ColumnInfo
import org.threeten.bp.LocalDate


data class MetricSimpleFutureWeatherEntry (
    @ColumnInfo(name = "date")
    override val date: LocalDate,
    @ColumnInfo(name = "avgtempC")
    override val avgTemperature: Double,
    @ColumnInfo(name = "condition_text")
    override val conditionText: String,
    @ColumnInfo(name = "condition_icon")
    override val conditionIconUrl: String,
    @ColumnInfo(name ="isDay")
    override val isDay: Int,
    @ColumnInfo(name ="condition_code")
    override val weatherIconCode: Int,
    @ColumnInfo(name = "time")
    override val time: String
) : UnitSpecificSimpleFutureWeatherEntry