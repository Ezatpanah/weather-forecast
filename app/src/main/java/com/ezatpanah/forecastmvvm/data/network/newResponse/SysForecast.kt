package com.ezatpanah.forecastmvvm.data.network.newResponse


import com.google.gson.annotations.SerializedName

data class SysForecast(
    @SerializedName("pod")
    var pod: String? = ""
)