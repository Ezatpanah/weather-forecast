package com.ezatpanah.forecastmvvm.data.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ezatpanah.forecastmvvm.data.db.entity.*
import com.ezatpanah.forecastmvvm.data.network.newResponse.NewCurrentWeatherResponse
import com.ezatpanah.forecastmvvm.data.network.newResponse.NewFutureWeatherResponse
import com.ezatpanah.forecastmvvm.data.network.response.CurrentWeatherResponse
import com.ezatpanah.forecastmvvm.data.network.response.ForecastDaysContainer
import com.ezatpanah.forecastmvvm.data.network.response.FutureWeatherResponse
import com.ezatpanah.forecastmvvm.internal.NoConnectivityException
import java.lang.Exception

const val FORECAST_DAYS_COUNT = 5

class WeatherNetworkDataSourceImpl(
        private val apiopenweathermapApiService: ApiOpenWeatherMapApiService
) : WeatherNetworkDataSource {

    private val _downloadedCurrentWeather = MutableLiveData<CurrentWeatherResponse>()
    override val downloadedCurrentWeather: LiveData<CurrentWeatherResponse>
        get() = _downloadedCurrentWeather

    override suspend fun fetchCurrentWeather(location: String, languageCode: String) {
        try {

            val model = if (locationIsString(location)) {

                val fetchedCurrentWeatherMetric = apiopenweathermapApiService
                        .getCurrentWeather(location, "metric")
                        .await()

                val fetchedCurrentWeatherImperial = apiopenweathermapApiService
                        .getCurrentWeather(location, "imperial")
                        .await()

                getCurrentWeatherResponse(fetchedCurrentWeatherMetric, fetchedCurrentWeatherImperial)
            } else {

                val spilitStr = location.split(",")
                val fetchedCurrentWeatherMetric = apiopenweathermapApiService
                        .getCurrentWeatherWithLatLon(spilitStr[0].toDouble(), spilitStr[1].toDouble() ,"metric")
                        .await()

                val fetchedCurrentWeatherImperial = apiopenweathermapApiService
                        .getCurrentWeatherWithLatLon(spilitStr[0].toDouble(), spilitStr[1].toDouble(), "imperial")
                        .await()

                getCurrentWeatherResponse(fetchedCurrentWeatherMetric, fetchedCurrentWeatherImperial)
            }


            _downloadedCurrentWeather.postValue(model)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection.", e)
        }
    }

    private fun locationIsString(location: String) =
            try {
                location.split(",")[0].toDouble()
                false
            } catch (e: Exception) {
                true
            }


    private val _downloadedFutureWeather = MutableLiveData<FutureWeatherResponse>()
    override val downloadedFutureWeather: LiveData<FutureWeatherResponse>
        get() = _downloadedFutureWeather

    override suspend fun fetchFutureWeather(
            location: String,
            languageCode: String
    ) {
        try {
            val model = if (locationIsString(location)) {
                val fetchedFutureWeatherMetric = apiopenweathermapApiService
                        .getFutureWeather(location, "metric")
                        .await()

                val fetchedFutureWeatherImperial = apiopenweathermapApiService
                        .getFutureWeather(location, "imperial")
                        .await()
                getFutureWeatherResponse(
                        metric = fetchedFutureWeatherMetric,
                        imperial = fetchedFutureWeatherImperial
                )
            } else {
                val spilitStr = location.split(",")

                val fetchedFutureWeatherMetric = apiopenweathermapApiService
                        .getFutureWeatherWithLotLon(spilitStr[0].toDouble(), spilitStr[1].toDouble() ,"metric")
                        .await()

                val fetchedFutureWeatherImperial = apiopenweathermapApiService
                        .getFutureWeatherWithLotLon(spilitStr[0].toDouble(), spilitStr[1].toDouble() ,"imperial")
                        .await()
                getFutureWeatherResponse(
                        metric = fetchedFutureWeatherMetric,
                        imperial = fetchedFutureWeatherImperial
                )
            }


            _downloadedFutureWeather.postValue(model)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection.", e)
        }
    }

    private fun getCurrentWeatherResponse(metric: NewCurrentWeatherResponse, imperial: NewCurrentWeatherResponse): CurrentWeatherResponse? {
        val location = WeatherLocation(
                name = metric.name,
                region = "N",
                country = metric.sys.country,
                lat = metric.coord.lat,
                lon = metric.coord.lon,
                tzId = metric.timezone.toString(),
                localtimeEpoch = metric.dt.toLong()
        )


        val condition = Condition(
                text = metric.weather[0].description,
                code = metric.weather[0].id,
                icon = metric.weather[0].icon
        )

        val currentWeatherEntry = CurrentWeatherEntry(
                tempC = metric.main.temp,
                tempF = imperial.main.temp,
                isDay = 1,
                condition = condition,
                feelslikeC = metric.main.feelsLike,
                feelslikeF = imperial.main.feelsLike,
                precipIn = 0.0,
                precipMm = 0.0,
                visKm = metric.visibility.toDouble(),
                visMiles = imperial.visibility.toDouble(),
                windDir = "N",
                windKph = metric.wind.speed,
                windMph = imperial.wind.speed
        )

        return CurrentWeatherResponse(
                location,
                currentWeatherEntry
        )
    }


    private fun getFutureWeatherResponse(
            metric: NewFutureWeatherResponse,
            imperial: NewFutureWeatherResponse
    ): FutureWeatherResponse? {
        val location = WeatherLocation(

                name = metric.city?.name!!,
                region = metric.city?.name!!,
                country = metric.city?.country!!,
                lat = metric.city?.coord?.lat!!,
                lon = metric.city?.coord?.lon!!,
                tzId = metric.city?.timezone!!.toString(),
                localtimeEpoch = metric.list[0].dt.toLong()
        )

        val entries = mutableListOf<FutureWeatherEntry>()

        for (i in 0..39) {

            entries.add(
                    FutureWeatherEntry(
                            date = metric.list[i].dtTxt,
                            time = metric.list[i].dtTxt,
                            day = Day(
                                    avgtempC = metric.list[i].main.temp,
                                    avgtempF = imperial.list[i].main.temp,
                                    avgvisKm = 0.0,
                                    avgvisMiles = 0.0,
                                    condition = Condition(
                                            text = metric.list[i].weather[0].description,
                                            code = metric.list[i].weather[0].id,
                                            icon = metric.list[i].weather[0].icon
                                    ),
                                    maxtempC = imperial.list[i].main.tempMax,
                                    maxtempF = imperial.list[i].main.tempMax,
                                    maxwindKph = metric.list[i].wind.speed,
                                    maxwindMph = imperial.list[i].wind.speed,
                                    mintempC = imperial.list[i].main.tempMin,
                                    mintempF = imperial.list[i].main.tempMin,
                                    totalprecipIn = 0.0,
                                    totalprecipMm = 0.0,
                                    uv = 0.0,
                                    isDay = if (metric.list[i].sys.pod == "d") 1 else 0
                            )
                    )
            )
        }

        val futureWeatherEntries = ForecastDaysContainer(
                entries = entries
        )

        return FutureWeatherResponse(
                futureWeatherEntries, location
        )
    }

}