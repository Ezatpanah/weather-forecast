package com.ezatpanah.forecastmvvm.data.provider

import com.ezatpanah.forecastmvvm.data.db.entity.WeatherLocation


interface LocationProvider {
    suspend fun hasLocationChanged(lastWeatherLocation: WeatherLocation): Boolean
    suspend fun getPreferredLocationString(): String
}