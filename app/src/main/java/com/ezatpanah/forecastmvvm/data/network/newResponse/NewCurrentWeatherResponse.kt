package com.ezatpanah.forecastmvvm.data.network.newResponse


import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tbl_new_current_weather")
data class NewCurrentWeatherResponse(
    @SerializedName("base")
    var base: String = "",
    @SerializedName("clouds")
    var clouds: Clouds,
    @SerializedName("cod")
    var cod: Int = 0,
    @SerializedName("coord")
    var coord: Coord,
    @SerializedName("dt")
    var dt: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("main")
    var main: Main,
    @SerializedName("name")
    var name: String,
    @SerializedName("sys")
    var sys: Sys,
    @SerializedName("timezone")
    var timezone: Int = 0,
    @SerializedName("visibility")
    var visibility: Int = 0,
    @SerializedName("weather")
    var weather: List<Weather>,
    @SerializedName("wind")
    var wind: Wind
)