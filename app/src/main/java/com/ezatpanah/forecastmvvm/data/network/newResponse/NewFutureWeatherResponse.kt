package com.ezatpanah.forecastmvvm.data.network.newResponse


import com.google.gson.annotations.SerializedName

data class NewFutureWeatherResponse(
    @SerializedName("city")
    var city: City? = null,

    @SerializedName("cnt")
    var cnt: Int? = 0,

    @SerializedName("cod")
    var cod: String? = "",

    @SerializedName("message")
    var message: Int? = 0,

    @SerializedName("list")
    var list : List<NewWeatherResponse>

)