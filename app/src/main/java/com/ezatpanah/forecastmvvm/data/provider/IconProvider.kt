package com.ezatpanah.forecastmvvm.data.provider

import com.ezatpanah.forecastmvvm.R

class IconProvider() {

    companion object {
        @JvmStatic
        fun getIconDrawableByCode(code: Int, isDay: Boolean): Int {
            return when (code) {
                200 -> if (isDay) R.drawable.ic_200d else R.drawable.ic_200n
                201 -> if (isDay) R.drawable.ic_201d else R.drawable.ic_201n
                202 -> if (isDay) R.drawable.ic_200d else R.drawable.ic_200n
                230 -> if (isDay) R.drawable.ic_230d else R.drawable.ic_230n
                231 -> if (isDay) R.drawable.ic_230d else R.drawable.ic_230n
                232 -> if (isDay) R.drawable.ic_230d else R.drawable.ic_230n
                233 -> if (isDay) R.drawable.ic_230d else R.drawable.ic_230n
                300 -> if (isDay) R.drawable.ic_300 else R.drawable.ic_300
                301 -> if (isDay) R.drawable.ic_300 else R.drawable.ic_300
                302 -> if (isDay) R.drawable.ic_300 else R.drawable.ic_300
                500 -> if (isDay) R.drawable.ic_500 else R.drawable.ic_500
                501 -> if (isDay) R.drawable.ic_500 else R.drawable.ic_500
                502 -> if (isDay) R.drawable.ic_502 else R.drawable.ic_502
                511 -> if (isDay) R.drawable.ic_511 else R.drawable.ic_511
                520 -> if (isDay) R.drawable.ic_511 else R.drawable.ic_511
                521 -> if (isDay) R.drawable.ic_521d else R.drawable.ic_521n
                522 -> if (isDay) R.drawable.ic_522 else R.drawable.ic_522
                600 -> if (isDay) R.drawable.ic_600d else R.drawable.ic_600n
                601 -> if (isDay) R.drawable.ic_601 else R.drawable.ic_601
                602 -> if (isDay) R.drawable.ic_601 else R.drawable.ic_601
                610 -> if (isDay) R.drawable.ic_610d else R.drawable.ic_610n
                611 -> if (isDay) R.drawable.ic_611 else R.drawable.ic_611
                612 -> if (isDay) R.drawable.ic_611 else R.drawable.ic_611
                621 -> if (isDay) R.drawable.ic_621d else R.drawable.ic_621n
                622 -> if (isDay) R.drawable.ic_622 else R.drawable.ic_622
                623 -> if (isDay) R.drawable.ic_622 else R.drawable.ic_622
                700 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                711 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                721 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                731 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                741 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                751 -> if (isDay) R.drawable.ic_700d else R.drawable.ic_700n
                800 -> if (isDay) R.drawable.ic_800d else R.drawable.ic_800n
                801 -> if (isDay) R.drawable.ic_801d else R.drawable.ic_801n
                802 -> if (isDay) R.drawable.ic_801d else R.drawable.ic_801n
                803 -> if (isDay) R.drawable.ic_803d else R.drawable.ic_803n
                804 -> if (isDay) R.drawable.ic_803d else R.drawable.ic_803n
                900 -> if (isDay) R.drawable.ic_900 else R.drawable.ic_900
                else -> R.drawable.ic_200d
            }

        }
    }
}