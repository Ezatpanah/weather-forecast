package com.ezatpanah.forecastmvvm.data.network.newResponse


import com.google.gson.annotations.SerializedName

data class Clouds(
    @SerializedName("all")
    var all: Int? = 0
)