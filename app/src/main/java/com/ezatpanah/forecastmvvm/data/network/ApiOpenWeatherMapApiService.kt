package com.ezatpanah.forecastmvvm.data.network

import com.ezatpanah.forecastmvvm.data.network.newResponse.NewCurrentWeatherResponse
import com.ezatpanah.forecastmvvm.data.network.newResponse.NewFutureWeatherResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query



const val API_KEY = "889180fc1c75d7198ac562b60d74312c"
//const val API_KEY = "62fc4256-8f8c-11e5-8994-feff819cdc9f"


interface ApiOpenWeatherMapApiService {

    @GET("/data/2.5/weather")
    fun getCurrentWeather(
            @Query("q") location: String,
            @Query("units") units : String
    ): Deferred<NewCurrentWeatherResponse>

    @GET("/data/2.5/weather")
    fun getCurrentWeatherWithLatLon(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("units") units : String
    ): Deferred<NewCurrentWeatherResponse>


    @GET("/data/2.5/forecast")
    fun getFutureWeather(
            @Query("q") location: String,
            @Query("units") units : String
    ): Deferred<NewFutureWeatherResponse>

    @GET("/data/2.5/forecast")
    fun getFutureWeatherWithLotLon(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("units") units : String
    ): Deferred<NewFutureWeatherResponse>



    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): ApiOpenWeatherMapApiService {
            val requestInterceptor = Interceptor { chain ->

                val url = chain.request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("appid", API_KEY)
                    .build()
                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                return@Interceptor chain.proceed(request)
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.openweathermap.org/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiOpenWeatherMapApiService::class.java)
        }
    }
}