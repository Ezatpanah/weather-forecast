package com.ezatpanah.forecastmvvm.data.db

import androidx.room.TypeConverter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter


object LocalDateConverter {
    @TypeConverter
    @JvmStatic
    fun stringToDate(str: String?) = str?.let {

        val array = str.split(" ")

        try {
            val newStr = array[0] + "T" + array[1]

            return@let LocalDate.parse(newStr, DateTimeFormatter.ISO_LOCAL_DATE_TIME)

        } catch (e : IndexOutOfBoundsException) {

            return@let LocalDate.parse(it, DateTimeFormatter.ISO_DATE)

        }
    }

    @TypeConverter
    @JvmStatic
    fun dateToString(dateTime: LocalDate?) = dateTime?.format(DateTimeFormatter.ISO_LOCAL_DATE)
}