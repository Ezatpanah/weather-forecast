package com.ezatpanah.forecastmvvm.data.provider

import com.ezatpanah.forecastmvvm.internal.UnitSystem


interface UnitProvider {
    fun getUnitSystem(): UnitSystem
}