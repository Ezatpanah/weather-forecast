package com.ezatpanah.forecastmvvm.data.network.newResponse


import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("coord")
    var coord: Coord? = null,
    @SerializedName("country")
    var country: String? = "",
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("sunrise")
    var sunrise: Int? = 0,
    @SerializedName("sunset")
    var sunset: Int? = 0,
    @SerializedName("timezone")
    var timezone: Int? = 0
)