package com.ezatpanah.forecastmvvm.internal


enum class UnitSystem {
    METRIC, IMPERIAL
}