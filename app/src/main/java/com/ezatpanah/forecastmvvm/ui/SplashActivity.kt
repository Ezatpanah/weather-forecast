package com.ezatpanah.forecastmvvm.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ezatpanah.forecastmvvm.R
import com.ezatpanah.forecastmvvm.internal.glide.GlideApp


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        val imageView: ImageView = findViewById(R.id.simpleImageView) as ImageView

        GlideApp.with(this)
                .load(R.raw.weathericon)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);




        Handler().postDelayed({
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        },8000)
    }
}
