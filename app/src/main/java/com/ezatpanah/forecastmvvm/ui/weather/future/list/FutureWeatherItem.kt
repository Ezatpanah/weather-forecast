package com.ezatpanah.forecastmvvm.ui.weather.future.list

import android.annotation.SuppressLint
import android.content.Context
import com.bumptech.glide.Glide
import com.ezatpanah.forecastmvvm.R
import com.ezatpanah.forecastmvvm.data.db.unitlocalized.future.list.MetricSimpleFutureWeatherEntry
import com.ezatpanah.forecastmvvm.data.db.unitlocalized.future.list.UnitSpecificSimpleFutureWeatherEntry
import com.ezatpanah.forecastmvvm.data.provider.IconProvider
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_future_weather.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle


class FutureWeatherItem(
    val weatherEntry: UnitSpecificSimpleFutureWeatherEntry,val context: Context
) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            textView_condition.text = weatherEntry.conditionText
            updateDate()
            updateTemperature()
            updateConditionImage()
        }
    }

    override fun getLayout() = R.layout.item_future_weather

    @SuppressLint("SetTextI18n")
    private fun ViewHolder.updateDate() {
        val dtFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
        textView_date.text = weatherEntry.date.format(dtFormatter)+ "\n" + weatherEntry.time.split(" ")[1]
    }

    private fun ViewHolder.updateTemperature() {
        val unitAbbreviation = if (weatherEntry is MetricSimpleFutureWeatherEntry) "°C"
        else "°F"
        textView_temperature.text = "${weatherEntry.avgTemperature.toInt()}$unitAbbreviation"
    }

    private fun ViewHolder.updateConditionImage() {
        val isDay = weatherEntry.isDay == 1
        val drawResourceId = IconProvider.getIconDrawableByCode(weatherEntry.weatherIconCode,isDay)

        val drawable = context.getDrawable(drawResourceId)


        Glide.with(context)
                .load(drawable)
                .into(imageView_condition_icon)
    }
}