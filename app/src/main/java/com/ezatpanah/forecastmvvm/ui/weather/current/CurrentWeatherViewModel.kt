package com.ezatpanah.forecastmvvm.ui.weather.current

import com.ezatpanah.forecastmvvm.data.provider.UnitProvider
import com.ezatpanah.forecastmvvm.data.repository.ForecastRepository
import com.ezatpanah.forecastmvvm.internal.lazyDeferred
import com.ezatpanah.forecastmvvm.ui.base.WeatherViewModel

class CurrentWeatherViewModel(
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : WeatherViewModel(forecastRepository, unitProvider) {

    val weather by lazyDeferred {
        forecastRepository.getCurrentWeather(super.isMetricUnit)
    }
}
