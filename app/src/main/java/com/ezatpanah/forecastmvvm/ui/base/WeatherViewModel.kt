package com.ezatpanah.forecastmvvm.ui.base

import androidx.lifecycle.ViewModel
import com.ezatpanah.forecastmvvm.data.provider.UnitProvider
import com.ezatpanah.forecastmvvm.data.repository.ForecastRepository
import com.ezatpanah.forecastmvvm.internal.UnitSystem
import com.ezatpanah.forecastmvvm.internal.lazyDeferred


abstract class WeatherViewModel(
    private val forecastRepository: ForecastRepository,
    unitProvider: UnitProvider
) : ViewModel() {

    private val unitSystem = unitProvider.getUnitSystem()

    val isMetricUnit: Boolean
        get() = unitSystem == UnitSystem.METRIC

    val weatherLocation by lazyDeferred {
        forecastRepository.getWeatherLocation()
    }
}